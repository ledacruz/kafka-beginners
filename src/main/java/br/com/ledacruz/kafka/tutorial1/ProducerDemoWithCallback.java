package br.com.ledacruz.kafka.tutorial1;

import java.util.Properties;

import org.apache.kafka.clients.producer.Callback;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.apache.kafka.common.serialization.StringSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ProducerDemoWithCallback {

	public static void main(String[] args) {

		String bootstrapServers = "127.0.0.1:9092";

		final Logger logger = LoggerFactory.getLogger(ProducerDemoWithCallback.class);

		// criar as propriedades do Producer
		Properties properties = new Properties();
		properties.setProperty(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);
		properties.setProperty(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
		properties.setProperty(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());

		// criar o producer
		// <key, value>
		KafkaProducer<String, String> producer = new KafkaProducer<String, String>(properties);

		for (int i = 0; i < 10; i++) {

			// criar um producer record
			ProducerRecord<String, String> record = new ProducerRecord<String, String>("first_topic", "hello world" + Integer.toString(i));

			// enviar o dado - assincrono

			producer.send(record, new Callback() {

				public void onCompletion(RecordMetadata recordMetadata, Exception exception) {
					// excecuta toda vez
					if (exception == null) {
						logger.info("Received new metadata." + "\n" + "Topic:" + recordMetadata.topic() + "\n"
								+ "Partition:" + recordMetadata.partition() + "\n" + "Offset:" + recordMetadata.offset()
								+ "\n" + "TimeStamp:" + recordMetadata.timestamp());
					} else {
						logger.error("Erro:" + exception);

					}

				}
			});
		}
		// flush data
		producer.flush();

		// flush and close producer
		producer.close();

	}

}
